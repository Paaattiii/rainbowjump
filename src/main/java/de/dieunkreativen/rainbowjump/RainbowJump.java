/*
* Rainbow Jump is a plugin for an easy Jump'n'Run system
* Copyright (C) Paaattiii <http://www.dieunkreativen.de>
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package de.dieunkreativen.rainbowjump;

import java.util.logging.Logger;

import mc.alk.arena.BattleArena;
import mc.alk.arena.objects.victoryconditions.VictoryType;

import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

import de.dieunkreativen.rainbowjump.Victory;

public class RainbowJump extends JavaPlugin {
	Logger log;
	static RainbowJump plugin;
	PluginDescriptionFile pdfFile = this.getDescription();
	
    @Override
    public void onLoad() {
            log = getLogger();
    }
	
	@Override
	public void onEnable() {
		plugin = this;
		log.info(pdfFile.getName() + " version " + pdfFile.getVersion() + " is enabled!");
		VictoryType.register(Victory.class, this);
		BattleArena.registerCompetition(this, "RainbowJump", "jump", RainbowJumpArena.class);

	}
	@Override
	public void onDisable() {
		log.info(pdfFile.getName() + " version " + pdfFile.getVersion() + " is disabled!");
	}
	
	public static RainbowJump getSelf() {
		return plugin;
	}
	
	
}
