/*
* Rainbow Jump is a plugin for an easy Jump'n'Run system
* Copyright (C) Paaattiii <http://www.dieunkreativen.de>
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package de.dieunkreativen.rainbowjump;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Sound;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.meta.FireworkMeta;

import mc.alk.arena.BattleArena;
import mc.alk.arena.objects.arenas.Arena;
import mc.alk.arena.objects.events.ArenaEventHandler;
import mc.alk.arena.objects.victoryconditions.VictoryCondition;

public class RainbowJumpArena extends Arena{
	
	int fireworkDelayTask;
	boolean isStarted;
	Map<Player, Location > checkPoints = new HashMap<Player, Location>();
	Victory scores;
	
	
    @Override
    public void onOpen() {
    	VictoryCondition vc = getMatch().getVictoryCondition(Victory.class);
    	scores = (Victory) (vc != null ? vc : new Victory(getMatch()));
    	getMatch().addVictoryCondition(scores);
    	scores.resetScores();
    }
	
	@Override
	public void onFinish() {
		checkPoints.clear();
		isStarted = false;
	}
	
	@Override
	public void onStart() {
		isStarted = true;
	}
	
	public void respawnPlayer(Player p) {
		if (checkPoints.get(p) == null) {
			p.teleport(getMatch().getSpawn(0, false).getLocation());
		}
		else {
			p.teleport(checkPoints.get(p));
		}
		
		addFail(p);
		p.setFireTicks(0);
	}
	
	
	public void addFail(Player p) {
		scores.addScore(BattleArena.toArenaPlayer(p), 1);
	}
	
	public void firework(final Player player) {
		Color color [] = {
				Color.WHITE,
				Color.RED,
				Color.AQUA,
				Color.GREEN,
				Color.BLUE,
				Color.YELLOW,
				Color.LIME,
				Color.ORANGE,
				Color.PURPLE
		};
		
		int size = color.length;
		int randomint = (int) Math.abs(Math.random()*size);
		
		
		Location location = player.getLocation();
	    final Firework firework = player.getWorld().spawn(location, Firework.class);
	    FireworkMeta data = (FireworkMeta) firework.getFireworkMeta();
	    data.addEffects(FireworkEffect.builder().trail(true).flicker(true).withColor(color[randomint]).with(Type.STAR).build());
	    data.setPower(0);
	    firework.setFireworkMeta(data);
	        	
	    fireworkDelayTask = Bukkit.getScheduler().scheduleSyncDelayedTask(RainbowJump.getSelf(), new Runnable() {
	    public void run() {
	    	firework.detonate();
	    	Bukkit.getScheduler().cancelTask(fireworkDelayTask);
	    	}
	    }, 3L); 
	 }

	@ArenaEventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		Player p = event.getPlayer();
		Location loc = p.getLocation();
		
		if (isStarted == false) {
			if (event.getTo().getBlockX() != event.getFrom().getBlockX() && event.getTo().getBlockZ() != event.getFrom().getBlockZ()) {
				event.setCancelled(true);
			}
		}
		
		if (loc.getBlock().getType() == Material.STATIONARY_WATER || loc.getBlock().getType() == Material.STATIONARY_LAVA) {
			respawnPlayer(p);
		}
		loc.setY(loc.getY() - 1);
		if (loc.getBlock().getType() == Material.COAL_BLOCK) {
			if (getMatch().getVictors().isEmpty()) {
				getMatch().setVictor(BattleArena.toArenaPlayer(p));
			}
			firework(p);
		}
	}
	
	@ArenaEventHandler
	 public void onEntityDamage(EntityDamageEvent event) {
	    if (event.getEntity() instanceof Player) {
	    	event.setCancelled(true);
	    }
	}
	
	@ArenaEventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent event) {
		event.setCancelled(true);
	}
	
	
	@ArenaEventHandler
	public void onButtonPressed(PlayerInteractEvent event) {
		if (event.getClickedBlock() != null) {
			Player p = event.getPlayer();
			if (event.getClickedBlock().getType() == Material.STONE_BUTTON && event.getClickedBlock().getLocation().distance(p.getLocation()) < 3) {
				p.sendMessage(ChatColor.GREEN + "== Checkpoint! ==");
				p.playSound(p.getLocation(), Sound.ENTITY_ARROW_HIT, 1F, 0);
				checkPoints.put(p, p.getLocation());
			}
		}
	}
	               
	

}